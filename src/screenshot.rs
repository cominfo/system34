#[path="./telegram.rs"]
mod telegram;

use log::error;
use std::process;
use tempfile::tempdir;

pub async fn take_screenshot(){
  match tempdir(){
    Err(e) => error!("Cannot create temp dir {}", e),
    Ok(dir) => {
      let filename = "screengrab.png";
      let path: String = dir.path().join(filename).to_str().unwrap_or(filename).into();
      let host: String = hostname::get().as_ref().ok().and_then(|h| h.to_str()).unwrap_or("`error: cannot find hostname`").into();
      if let Err(e) = process::Command::new("boxcutter.exe").args(["-f", &path].iter()).output() {
        error!("Cannot take screenshot: {}", e);
      } else if let Err(e) = telegram::send_photo(&path, &format!("Papercut chiuso sulla macchina {}", host)).await {
        error!("Cannot send photo: {}", e);
      }
    }
  }
}