#![windows_subsystem = "windows"]
#![cfg(target_os = "windows")]

mod screenshot;

use std::{process, thread, time};
use log::{error, warn};
use simplelog::*;
use std::fs::File;


const EXECUTABLE: &str = r"C:\PaperCut NG Client\pc-client.exe";
const TITLE: &str = "ComInfo sicurezza";
const MESSAGE: &str = "Ah Ah! Sei stato beccato!\nPensavi che chiudendo Papercut non avresti pagato la stampa, ma non è così!\nOra le tue doghe verranno prelevate.";

#[tokio::main]
async fn main() -> Result<(),()> {
  let log_config = ConfigBuilder::new().set_time_format_str("%_d %b %Y %_H:%M:%S %Z").set_time_to_local(true).build();
  let log_level = LevelFilter::Info;
  match File::create("system34.log") {
    Ok(file) => {let _ = WriteLogger::init(log_level, log_config, file);},
    Err(e) => {
      eprintln!("Cannot open log file: {}", e);
      let _ = TermLogger::new(log_level, log_config, TerminalMode::Stderr, ColorChoice::Auto);
    }
  };
  service_logic()
}

fn service_logic() -> Result<(),()> {
  let mut errors: u32 = 0;
  let mut handle: Option<process::Child> = None;
  loop {
    if let Err(e) = process::Command::new(EXECUTABLE).spawn().and_then(|mut h| h.wait()) {
      error!("System34 error: {}", e);
      thread::sleep(time::Duration::from_secs(5));
      errors += 1;
      if errors > 10 {
        error!("Aborting due to too much errors.");
        return Err(());
      }
    } else {
      tokio::spawn(screenshot::take_screenshot());
      warn!("Main program closed. Showing message box.");
      handle.and_then(|mut h| h.kill().ok());
      handle = process::Command::new("msgbox.exe").args([TITLE, MESSAGE, "error"].iter()).spawn().map_err(|e| error!("Error opening msgbox: {}", e)).ok();
    }
  }
}