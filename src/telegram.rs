use telegram_bot::{Api, InputFileUpload, Error, ChatId, CanSendDocument};

pub async fn send_photo(photo: &str, caption: &str) -> Result<(), Error>{
  let file = InputFileUpload::with_path(photo);
  let api = Api::new(env!("TELEGRAM_BOT_TOKEN"));
  let chat = ChatId::new(-1001320286257);
  api.send(chat.document(file).caption(caption)).await.and(Ok(()))
}