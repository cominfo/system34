#![windows_subsystem = "windows"]

use msgbox::{IconType, MsgBoxError};
use std::env::args;

fn main() -> Result<(), MsgBoxError> {
  let argv: Vec<_> = args().into_iter().chain(vec![String::new(); 3].into_iter()).collect();
  msgbox::create(&argv[1], &argv[2], match &*argv[3]{
    "error" => IconType::Error,
    "info" => IconType::Info,
    _ => IconType::None
  })
}
