# System 34

Questo programma si camuffa da app di sistema per evitare venga chiuso da chi vuole fregare ComInfo, ma in realtà avvia Papercut e appena questo viene chiuso (nel tentativo di non pagare) lo riavvia. Fa anche uno screenshoot che manda nel canale Telegram del bot della stampante.

## Come installarlo

1. Scarica il programma compilato da GitLab: https://gitlab.com/cominfo/system34/-/jobs/artifacts/main/download?job=windows-mingw-64
2. Copia i tre eseguibili in `C:\Windows\System34` (cartella da creare)
3. Mettere in avvio automatico `C:\Windows\System34\system34.exe`:
   1. Premi Super+R (Super è il tasto col logo di Winzoz)
   2. Digita `shell:startup` e premi Ok
   3. Copia nella cartella che si aprirà il collegamento a `system34.exe`

## Importante

1. Non mettere Papercut client in avvio automatico (rimuover in caso lo sia)
2. Papercut client deve trovarsi alla seguente posizione: `C:\PaperCut NG Client\pc-client.exe`
